package com.example.user.service.service.implementation;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.stereotype.Service;

import com.example.user.service.dto.ResponseDto;
import com.example.user.service.dto.UserDto;
import com.example.user.service.entity.User;
import com.example.user.service.exception.ResourceConflictException;
import com.example.user.service.exception.ResourceNotFound;
import com.example.user.service.repository.UserRepository;
import com.example.user.service.service.UserService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService{

	private final UserRepository userRepository;
	
	public ResponseDto addUser(UserDto userDto)
	{
		Optional<User> user=userRepository.findByEmailId(userDto.getEmailId());
		if(user.isPresent())
		{
			throw new ResourceConflictException("User Already exists");
		}
		User users=new User();
		BeanUtils.copyProperties(userDto, users);
		userRepository.save(users);
		return new ResponseDto("User added successfully");
		
	}
	
	public List<User> getUsers()
	{
		return userRepository.findAll();
		
	}
	
	public UserDto getUser(int id)
	{
		User user= userRepository.findById(id).orElseThrow(() ->
		new ResourceNotFound("No user Exists")
				);
		UserDto userDto=new UserDto();
		BeanUtils.copyProperties(user, userDto);
		
		return userDto;
		
	}
	
	
}
