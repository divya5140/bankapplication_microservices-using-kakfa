package com.example.fundtransfer.service.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.example.fundtransfer.service.entity.FundTransfer;

public interface FundTransferRepository extends JpaRepository<FundTransfer, Long>{

}
