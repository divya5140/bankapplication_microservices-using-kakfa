package com.example.fundtransfer.service.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class TransactionDto {

	private long fromAccountNumber;

	private long toAccountNumber;

	private String transferType;

	private long fundtransferId;

	private double amount;

}
