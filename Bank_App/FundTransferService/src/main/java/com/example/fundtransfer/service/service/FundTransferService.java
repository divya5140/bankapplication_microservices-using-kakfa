package com.example.fundtransfer.service.service;

import com.example.fundtransfer.service.dto.FundTransferRequest;
import com.example.fundtransfer.service.dto.ResponseDto;

public interface FundTransferService {
	
	ResponseDto fundTransfer(FundTransferRequest fundTransferRequest);

}
