package com.example.fundtransfer.service.entity;

import java.time.LocalDate;

import org.hibernate.annotations.CreationTimestamp;

import jakarta.persistence.Entity;
import jakarta.persistence.EnumType;
import jakarta.persistence.Enumerated;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Entity
public class FundTransfer {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private long fundtransferId;
	
	
	private long fromAccountNumber;
	
	private long toAccountNumber;
	
	private double amount;
	
	@Enumerated(EnumType.STRING)
	private TransferType transferType;
	
	@CreationTimestamp
	private LocalDate transferAt;


}
