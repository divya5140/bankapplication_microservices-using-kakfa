package com.example.fundtransfer.service.dto;

public record ResponseDto(String message) {
}