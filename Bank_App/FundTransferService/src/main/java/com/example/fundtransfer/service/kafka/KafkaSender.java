package com.example.fundtransfer.service.kafka;

import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.stereotype.Service;

import com.example.fundtransfer.service.dto.TransactionDto;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class KafkaSender {
	
	@SuppressWarnings({ "unchecked", "rawtypes", "resource" })
	public void send(TransactionDto transactionDto) {
		
		Properties props = new Properties();
		props.put("bootstrap.servers", "localhost:9092");
		Producer<String, TransactionDto> kafkaProducer = new KafkaProducer<>(props, new StringSerializer(),
				new JsonSerializer());	
		
		log.info("Message Sent: "+transactionDto);
		System.out.println();
		ProducerRecord<String, TransactionDto> record = new ProducerRecord<String, TransactionDto>("exception", transactionDto);
		log.info(""+record);
		System.out.println(kafkaProducer.send(record));
		
 
		
	}
 
 
}


