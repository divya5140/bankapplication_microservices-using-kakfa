package com.example.account.service.exception;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class GlobalException extends RuntimeException{

    private String errorMessage;

    private String errorCode;

    public GlobalException(String message) {
        super(message);
    }
}
