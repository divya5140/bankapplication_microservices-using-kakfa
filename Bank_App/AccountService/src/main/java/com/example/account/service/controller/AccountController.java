package com.example.account.service.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.account.service.dto.AccountDto;
import com.example.account.service.dto.ResponseDto;
import com.example.account.service.service.AccountService;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class AccountController {

	private final AccountService accountService;

	@PostMapping
	public ResponseEntity<ResponseDto> createAccount(@RequestBody AccountDto accountDto) {
		return new ResponseEntity<>(accountService.createAccount(accountDto), HttpStatus.CREATED);
	}
	
	
	@GetMapping("/account")
	public AccountDto getAccount(@RequestParam long accountNumber) {
		return accountService.getAccountNumber(accountNumber);
	}

	@PutMapping("/account")
	public AccountDto updateAccount(@RequestBody AccountDto accountDto) {
		return accountService.updateAccount(accountDto);
	}
	
	@GetMapping("/balance")
	public Double getBalance(@RequestParam long accountNumber) {
		return accountService.getBalance(accountNumber);
	}
}
