package com.example.fundtransfer.service.service.implementation;

import java.util.Objects;

import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.fundtransfer.service.dto.AccountDto;
import com.example.fundtransfer.service.dto.FundTransferRequest;
import com.example.fundtransfer.service.dto.ResponseDto;
import com.example.fundtransfer.service.dto.TransactionDto;
import com.example.fundtransfer.service.entity.FundTransfer;
import com.example.fundtransfer.service.entity.TransferType;
import com.example.fundtransfer.service.exception.ResourceNotFound;
import com.example.fundtransfer.service.external.AccountService;
import com.example.fundtransfer.service.kafka.KafkaSender;
import com.example.fundtransfer.service.repository.FundTransferRepository;
import com.example.fundtransfer.service.service.FundTransferService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class FundTransferServiceImpl implements FundTransferService {
	
	private final FundTransferRepository fundTransferRepository;
	
	private final AccountService accountService;
	
	private final KafkaSender kafkaSender;
	
	public ResponseDto fundTransfer(FundTransferRequest fundTransferRequest)
	{
		
		ResponseEntity<AccountDto> accountDto1=accountService.getAccount(fundTransferRequest.getFromAccountNumber());
		if(Objects.isNull(accountDto1.getBody()))
		{
			throw new ResourceNotFound("Account not found");
		}
		ResponseEntity<AccountDto> accountDto2=accountService.getAccount(fundTransferRequest.getToAccountNumber());
		if(Objects.isNull(accountDto2.getBody()))
		{
			throw new ResourceNotFound("Account not found");
		}
		FundTransfer fundTransfer=FundTransfer.builder()
				.fromAccountNumber(fundTransferRequest.getFromAccountNumber())
		.toAccountNumber(fundTransferRequest.getToAccountNumber())
		.amount(fundTransferRequest.getAmount())
	     .transferType(TransferType.valueOf(fundTransferRequest.getTransferType()))
		
		.build();
		fundTransferRepository.save(fundTransfer);
		TransactionDto dto=TransactionDto.builder()
				.fromAccountNumber(fundTransfer.getFromAccountNumber())
				.toAccountNumber(fundTransfer.getToAccountNumber())
				.transferType(TransferType.valueOf(fundTransfer.getTransferType())))
				.fundtransferId(fundTransfer.getFundtransferId())
				.amount(fundTransfer.getAmount())
				.build();
		
		kafkaSender.send(dto);
		
		return new ResponseDto("Fund Transfer successful");
	}
	}
	
	


