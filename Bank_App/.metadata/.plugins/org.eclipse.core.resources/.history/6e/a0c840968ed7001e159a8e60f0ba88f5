package com.example.account.service.service.implementation;

import java.security.SecureRandom;
import java.util.Objects;

import org.springframework.beans.BeanUtils;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.account.service.dto.AccountDto;
import com.example.account.service.dto.ResponseDto;
import com.example.account.service.dto.UserDto;
import com.example.account.service.entity.Account;
import com.example.account.service.entity.AccountType;
import com.example.account.service.exception.ResourceConflictException;
import com.example.account.service.exception.ResourceNotFound;
import com.example.account.service.external.UserService;
import com.example.account.service.repository.AccountRepository;
import com.example.account.service.service.AccountService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class AccountServiceImpl implements AccountService{
	
	private final AccountRepository accountRepository;
	
	private final UserService userService;
	
	
	public ResponseDto createAccount(AccountDto accountDto)
	
	{
		ResponseEntity<UserDto> userDto=userService.getUser(accountDto.userId());
		if(Objects.isNull(userDto.getBody()))
		{
			throw new ResourceNotFound("User not found");
		}
		accountRepository.findAccountByUserIdAndAccounttype(accountDto.userId(), AccountType.valueOf(accountDto.accounttype())).
        ifPresent(account -> {
            log.error("Account already exists ");
            throw new ResourceConflictException("Account already exists");
        });
		Account accounts = new Account();
		SecureRandom secureRandom = new SecureRandom();
		
		
		
		BeanUtils.copyProperties(accountDto, accounts);
		accounts.setAccounttype(AccountType.valueOf(accountDto.accounttype()));
		accounts.setAccountNumber(secureRandom.nextLong(999999 - 100000 + 1L) + 1);
		accountRepository.save(accounts);
		return new ResponseDto("Account added successfully");


	
	}
	
	
	public Account getAccountNumber(long accountNumber)
	{
		return accountRepository.findAccountByAccountNumber(accountNumber);
		
	}

}