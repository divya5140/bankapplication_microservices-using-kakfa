package com.example.transaction.service.kafka;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.http.ResponseEntity;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.example.transaction.service.entity.AccountDto;
import com.example.transaction.service.entity.Transaction;
import com.example.transaction.service.entity.TransferType;
import com.example.transaction.service.external.AccountService;
import com.example.transaction.service.repository.TransactionRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class KafkaReceiver {

	private final TransactionRepository transactionRepository;

	private final AccountService accountService;

	@KafkaListener(topics = "exception", groupId = "group-id")
	public void consume(ConsumerRecord<String, Object> records) throws JsonMappingException, JsonProcessingException {

		Transaction transaction1 = new Transaction();
		Transaction transaction2 = new Transaction();
		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.readTree(records.value().toString());
		Long fromAccountNumber = node.get("fromAccountNumber").asLong();
		Long toAccountNumber = node.get("toAccountNumber").asLong();

		String transactionTypeString = node.get("transferType").asText();
		Long fundTransferId = node.get("fundtransferId").asLong();
		Double amount = node.get("amount").asDouble();

		TransferType transferType = TransferType.valueOf(transactionTypeString);
		if (transferType == TransferType.DEBIT) {
			transaction1.setTransferType(transferType);
			transaction1.setFundtransferId(fundTransferId);
			transaction1.setAccountNumber(fromAccountNumber);
			transaction2.setTransferType(TransferType.CREDIT);
			transaction2.setFundtransferId(fundTransferId);
			transaction2.setAccountNumber(toAccountNumber);
			ResponseEntity<AccountDto> accountDto = accountService.getAccount(fromAccountNumber);
			accountDto.getBody().setBalance(accountDto.getBody().getBalance() - amount);
			ResponseEntity<AccountDto> accountDto1 = accountService.getAccount(toAccountNumber);
			accountDto.getBody().setBalance(accountDto.getBody().getBalance() + amount);
			accountService.updateAccount(accountDto.getBody());
			accountService.updateAccount(accountDto1.getBody());

		} else {
			transaction1.setTransferType(transferType);
			transaction1.setFundtransferId(fundTransferId);
			transaction1.setAccountNumber(fromAccountNumber);
			transaction2.setTransferType(transferType.DEBIT);
			transaction2.setFundtransferId(fundTransferId);
			transaction2.setAccountNumber(toAccountNumber);
			ResponseEntity<AccountDto> accountDto = accountService.getAccount(fromAccountNumber);
			accountDto.getBody().setBalance(accountDto.getBody().getBalance() + amount);
			ResponseEntity<AccountDto> accountDto1 = accountService.getAccount(toAccountNumber);
			accountDto.getBody().setBalance(accountDto.getBody().getBalance() - amount);
			accountService.updateAccount(accountDto.getBody());
			accountService.updateAccount(accountDto1.getBody());
		}

		transactionRepository.save(transaction1);
		transactionRepository.save(transaction2);

		log.info(records.value() + "");
	}

}
