package com.example.transaction.service.kafka;

import java.util.Objects;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import com.example.transaction.service.entity.AccountDto;
import com.example.transaction.service.entity.Transaction;
import com.example.transaction.service.entity.TransferType;
import com.example.transaction.service.exception.ResourceNotFound;
import com.example.transaction.service.external.AccountService;
import com.example.transaction.service.repository.TransactionRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class KafkaReceiver {

	private final TransactionRepository transactionRepository;

	private final AccountService accountService;

	@KafkaListener(topics = "exception", groupId = "group-id")
	public void consume(ConsumerRecord<String, Object> records) throws JsonMappingException, JsonProcessingException {

		Transaction transaction1 = new Transaction();
		Transaction transaction2 = new Transaction();

		ObjectMapper mapper = new ObjectMapper();
		JsonNode node = mapper.readTree(records.value().toString());
		Long fromAccountNumber = node.get("fromAccountNumber").asLong();
		Long toAccountNumber = node.get("toAccountNumber").asLong();
		String transactionTypeString = node.get("transferType").asText();
		Long fundTransferId = node.get("fundtransferId").asLong();
		Double amount = node.get("amount").asDouble();

		System.out.println(amount);

		TransferType transferType = TransferType.valueOf(transactionTypeString);
		System.out.println(transferType);

		if (transactionTypeString.equals(TransferType.DEBIT.toString())) {

			transaction1.setFundtransferId(fundTransferId);
			transaction1.setAccountNumber(fromAccountNumber);
			transaction1.setTransferType(TransferType.DEBIT);

			transaction2.setTransferType(TransferType.CREDIT);
			transaction2.setFundtransferId(fundTransferId);
			transaction2.setAccountNumber(toAccountNumber);
			System.out.println(fromAccountNumber);

			AccountDto accountDto = accountService.getAccount(fromAccountNumber);
			System.out.println(accountDto);
			if (Objects.isNull(accountDto)) {
				throw new ResourceNotFound("fromAccount not found");
			}

			accountDto.setBalance(accountDto.getBalance() - amount);

			AccountDto accountDto1 = accountService.getAccount(toAccountNumber);
			System.out.println(accountDto1);

			if (Objects.isNull(accountDto1)) {
				throw new ResourceNotFound("toAccount not found");
			}
			accountDto1.setBalance(accountDto1.getBalance() + amount);
			System.out.println(accountDto.getBalance());
			System.out.println(accountDto1.getBalance());
			accountService.updateAccount(accountDto);
			accountService.updateAccount(accountDto1);

		} else if (transactionTypeString.equals(TransferType.CREDIT.toString())) {

			transaction1.setTransferType(transferType);
			transaction1.setFundtransferId(fundTransferId);
			transaction1.setAccountNumber(fromAccountNumber);
			transaction2.setTransferType(TransferType.DEBIT);
			transaction2.setFundtransferId(fundTransferId);
			transaction2.setAccountNumber(toAccountNumber);

			AccountDto accountDto = accountService.getAccount(fromAccountNumber);
			accountDto.setBalance(accountDto.getBalance() + amount);

			AccountDto accountDto1 = accountService.getAccount(toAccountNumber);
			accountDto1.setBalance(accountDto1.getBalance() - amount);

			System.out.println(accountDto.getBalance());
			System.out.println(accountDto1.getBalance());
			accountService.updateAccount(accountDto);
			accountService.updateAccount(accountDto1);
		} else {
			System.out.println("Give proper TransferType");
		}

		transactionRepository.save(transaction1);
		transactionRepository.save(transaction2);

		log.info(records.value() + "");
	}

}
