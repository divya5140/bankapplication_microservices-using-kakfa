package com.example.transaction.service.service;

import java.util.List;

import com.example.transaction.service.entity.Transaction;

public interface TransactionService {

	List<Transaction> getTransactions(long accountNumber);
}
