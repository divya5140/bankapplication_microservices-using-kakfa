package com.example.transaction.service.configuration;

import org.springframework.cloud.openfeign.FeignClientProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import feign.codec.ErrorDecoder;

@Configuration
public class FeignConfiguration extends FeignClientProperties.FeignClientConfiguration{

	
	 @Bean
	    ErrorDecoder errorDecoder() {
	        return new FeignClientErrorDecoder();
	    }

	
}
