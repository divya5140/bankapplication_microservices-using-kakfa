package com.example.transaction.service.entity;

import lombok.Data;

@Data
public class AccountDto {
	
private int accountId;
	
	private long accountNumber;
	
	
	private String accounttype;
	
	private double balance ;
	
	private int userId;

}
