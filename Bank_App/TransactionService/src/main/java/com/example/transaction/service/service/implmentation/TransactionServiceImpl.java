package com.example.transaction.service.service.implmentation;

import java.util.List;

import org.springframework.stereotype.Service;

import com.example.transaction.service.entity.Transaction;
import com.example.transaction.service.repository.TransactionRepository;
import com.example.transaction.service.service.TransactionService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@RequiredArgsConstructor
public class TransactionServiceImpl implements TransactionService {
	
	private final TransactionRepository transactionRepository; 
	
	public List<Transaction> getTransactions(long accountNumber) {	
		return transactionRepository.findByAccountNumber(accountNumber);
	}
 
}
